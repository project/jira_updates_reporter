CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

-- INTRODUCTION --
* This module is built to add drupal core and modules updates to your JIRA project.

-- REQUIREMENTS --

* Requires Drupal Update Status to be enabled.

-- INSTALLATION --
* Install as usual, see:
  https://www.drupal.org/docs/extending-drupal/installing-modules
  for further information.


-- CONFIGURATION --
* Set the configurations in /jira-updates-reporter/config

-- Maintainers --
Current maintainers:
* Ahmed Alhaddad (ahmedx) - https://www.drupal.org/u/ahmedx