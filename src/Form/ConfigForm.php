<?php

namespace Drupal\jira_updates_reporter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\jira_updates_reporter\JiraWrapperService;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase
{

  /**
   * Jira API Wrapper.
   *
   * @var \Drupal\jira_updates_reporter\JiraWrapperService
   */
  protected $jiraWrapperService;

  /**
   * Class constructor.
   */
  public function __construct(JiraWrapperService $jira_wrapper_service)
  {
    $this->jiraWrapperService = $jira_wrapper_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('jira_updates_reporter_wrapper_service')
    );
  }


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'jira_updates_reporter.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'jira_updates_reporter_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $config = $this->config('jira_updates_reporter.config');

    $form['authorization'] = [
      '#type'  => 'details',
      '#open'  => true,
      '#title' => $this->t('Jira Authorization'),
    ];


    $form['authorization']['jira_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Jira URL'),
      '#description' => $this->t('Jira Url. Ex: https://domain.atlassian.net'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => $config->get('jira_url') ?? '',
      '#required' => true,
    ];
    $form['authorization']['jira_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Jira Username'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => $config->get('jira_username') ?? '',
      '#required' => true,
    ];
    $form['authorization']['jira_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Jira token'),
      '#description' => $this->t('Jira password (Token)'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => $config->get('jira_token') ?? '',
      '#required' => true,
    ];

    $form['project'] = [
      '#type'  => 'details',
      '#open'  => true,
      '#title' => $this->t('Jira Project'),
    ];


    $form['project']['project_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project key'),
      '#description' => $this->t('Jira Project Key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => $config->get('project_key') ?? '',
      '#required' => true,
    ];
    $form['project']['issuetype_name_release'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IssueType name (Release)'),
      '#description' => $this->t('Jira IssueType name for release updates to be assigned'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => $config->get('issuetype_name_release') ?? '',
      '#required' => true,
    ];
    $form['project']['issuetype_name_security'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IssueType name (Security)'),
      '#description' => $this->t('Jira IssueType name for security updates to be assigned'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => $config->get('issuetype_name_security') ?? '',
      '#required' => true,
    ];
    $form['project']['security_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Security updates only'),
      '#default_value' => $config->get('security_only') ?? '',
    ];
    $form['project']['check_on_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check for updates on cron'),
      '#default_value' => $config->get('check_on_cron') ?? '',
    ];
    $form['project']['last_check'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Check'),
      '#description' => $this->t('Last check'),
      '#weight' => '0',
      '#disabled' => true,
      '#default_value' => $config->get('last_check') ?? '',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form['updateJira'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save and update'),
      '#submit' => ['::submitForm', '::updateJira'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $config = $this->config('jira_updates_reporter.config');
    $save_config = array(
      "jira_url", "jira_username", "jira_token",
      "project_key", "issuetype_name_release",
      "issuetype_name_security", "security_only",
      "check_on_cron", "last_check"
    );
    foreach ($form_state->getValues() as $key => $value) {
      if (in_array($key, $save_config)) {
        $config->set($key, $value);
      }
    }
    $config->save();
  }


  public function updateJira(array &$form, FormStateInterface $form_state)
  {
    $this->jiraWrapperService->reportToJira();
  }
}
