<?php

namespace Drupal\jira_updates_reporter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;



/**
 * Class JiraWrapperService.
 *
 * @package Drupal\jira_updates_reporter
 */
class JiraWrapperService
{
    use StringTranslationTrait;


    /**
     * @var \Drupal\Core\Logger\LoggerChannelInterface
     */
    protected $loggerJira;

    /**
     * The config factory.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * JiraWrapper constructor.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The config factory service.
     * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
     *   Logger Factory service.
     */
    public function __construct(
        ConfigFactoryInterface $config_factory,
        LoggerChannelFactoryInterface $logger_factory
    ) {
        $this->configFactory = $config_factory->getEditable('jira_updates_reporter.config');
        $this->loggerJira = $logger_factory->get('jira_updates_reporter');
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            'jira_updates_reporter.config',
        ];
    }


    /**
     * Get Jira Issues.
     *
     * @param string $jql
     *   JQL query.
     *
     * @return array
     *   Array of issues.
     */
    public function getIssues($jql)
    {
        $issues = [];
        $jiraUrl = $this->configFactory->get('jira_url');
        $jiraUsername = $this->configFactory->get('jira_username');
        $jiraPassword = $this->configFactory->get('jira_token');
        $jiraApiUrl = $jiraUrl . '/rest/api/latest/search';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $jiraApiUrl);
        curl_setopt($ch, CURLOPT_USERPWD, $jiraUsername . ':' . $jiraPassword);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jql);
        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($result, true);
        if ($httpCode == 200) {
            $issues = $result['issues'];
        } else {
            if (isset($result['errorMessages']) && $result['errorMessages']) {
                $this->loggerJira->warning('Jira Updates Reporter: ' . print_r($result['errorMessages'], true));
            }
            $this->loggerJira->error($this->t('Error while getting issues from Jira. Error code: @code', ['@code' => $httpCode]));
        }
        return $issues;
    }

    /**
     * Post Jira Issue.
     *
     * @param array $data
     *   Issue data.
     *
     * @return array
     *   Issue details.
     */
    public function postIssue($data)
    {
        $jiraUrl = $this->configFactory->get('jira_url');
        $jiraUsername = $this->configFactory->get('jira_username');
        $jiraPassword = $this->configFactory->get('jira_token');
        $jiraApiUrl = $jiraUrl . '/rest/api/2/issue';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $jiraApiUrl);
        curl_setopt($ch, CURLOPT_USERPWD, $jiraUsername . ':' . $jiraPassword);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($result, true);
        if ($httpCode == 201) {
            $this->loggerJira->info('Jira Updates Reporter: ' . print_r($result, true));
            \Drupal::messenger()->addStatus(t('Successfully created issue: ' . $result['key']));
            return $result;
        } else {
            if (isset($result['errorMessages']) && $result['errorMessages']) {
                $this->loggerJira->warning('Jira Updates Reporter: ' . print_r($result['errorMessages'], true));
            }
            $this->loggerJira->error($this->t('Error while posting issue to Jira. Error code: @code', ['@code' => $httpCode]));
            return false;
        }
    }

    /**
     * Report to JIRA function.
     */
    public function reportToJira()
    {
        update_refresh();
        update_fetch_data();

        $date_time = new \DateTime('now');
        $formatted_date = $date_time->format('Y-m-d H:i:s');

        $this->configFactory->set('last_check', $formatted_date);
        $this->configFactory->save();

        if ($available = update_get_available(TRUE)) {
            $projects = update_calculate_project_data($available);
            $project_key = $this->configFactory->get('project_key');

            foreach ($projects as $project) {
                if (
                    isset($project['recommended']) && !empty($project['recommended'])
                    && isset($project['info']['version']) && !empty($project['info']['version'])
                    && $project['info']['version'] <> $project['recommended']
                ) {
                    $security_only = $this->configFactory->get('security_only');
                    if ($project['status'] == 1 || (!$security_only && $project['status'] == 4)) {
                        $summary = 'Update ' . $project['title'] . ' to ' . $project['recommended'];
                        $description = $summary . ' ' . $project['link'];

                        if ($project['status'] == 1) {
                            $summary = 'SECURITY ' . $summary;
                            $issue_type_name = $this->configFactory->get('issuetype_name_security');
                        }
                        if ($project['status'] == 4) {
                            $summary = 'RELEASE ' . $summary;
                            $issue_type_name = $this->configFactory->get('issuetype_name_release');
                        }
                        $jql = json_encode(['jql' => 'Summary ~ "' . $summary . '" AND project = "' . $project_key . '"']);
                        $found_issues = $this->getIssues($jql);
                        if (empty($found_issues)) {
                            $issue_data = array(
                                'fields' => array(
                                    'project'     => array('key' => $project_key),
                                    'summary'     => $summary,
                                    'description' => $description,
                                    'issuetype'   => array('name' => $issue_type_name),
                                ),
                            );
                            $this->postIssue($issue_data);
                        }
                    }
                }
            }
        }
    }
}
